trait RNG { def nextInt: (Int,RNG) }
case class SimpleRNG(seed: Long) extends RNG {
   def nextInt: (Int, RNG) = {
     val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
     val nextRNG = SimpleRNG(newSeed)
     val n = (newSeed >>> 16).toInt
     (n, nextRNG)
     }
  }
def nonNegativeInt(rng: RNG): (Int, RNG) = {
  val (i,r) = rng.nextInt
   (math.abs(i),r)
   }
type Rand[+A] = RNG => (A, RNG)
val int: Rand[Int] = _.nextInt
def unit[A](a: A): Rand[A] = (a, _)
def lift[A,B](f: A => B): Option[A] => Option[B] = for(a<- _) yield f(a)
def map2[A,B,C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] = for(ab<- a; bc<- b) yield f(ab,bc)
trait Monad[M[_]] {
  def unit[A](a: A): M[A]
  def flatMap[A, B](fa: M[A])(f: A => M[B]): M[B] =
}